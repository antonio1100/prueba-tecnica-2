<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'AutenticacionesController@showLoginForm')->name('login')->middleware('guest');
Route::get('login', 'AutenticacionesController@showLoginForm')->name('login')->middleware('guest');
Route::post('login', 'AutenticacionesController@authenticate')->name('login');
Route::post('iniciar-sesion', 'AutenticacionesController@iniciarSesion')->name('inicio');
Route::get('session', 'AutenticacionesController@datosUsuario')->name('getsession');
// Route::get('session', 'ImportController@datosUsuario')->name('getsession');
Route::post('logout', 'AutenticacionesController@logout')->name('logout');


// Auth::logout()

Route::get('/home', function () {
    return redirect('/dashboard');
})->name('home');

Route::get('/', function () {
    return redirect('/login');
})->name('inicio');



Route::get('/{any}', 'HomeController@index')->where('any','dashboard|proveedores|importar|home|registroUsuario|getSucursales|getEmpleados')->name('app');


//Importar excel
Route::post('/importarExcel', 'ImportController@importExcel')->name('importExcel');

//Obtener proveedores de la BD
Route::get('/registros-proveedor', 'ImportController@getProveedores')->name('proveedores');
//Agregar nuevo proveedor
Route::post('/newRegistro', 'ImportController@newProveedor')->name('newUsuario');


//Agregar nuevo usuario
Route::post('/newUsuario', 'ImportController@newUsuario')->name('newUsuario');
//Obtener usuarios de la BD
Route::get('/usuarios', 'ImportController@obtenerUsuarios')->name('usuarios');
//Actualizar datos de usuario especifico
Route::post('/usuario-edit/{id}', 'ImportController@actualizarUsuario')->name('usuarioUpdate');
//Eliminar usuario especifico
Route::post('/usuario-delete/{id}', 'ImportController@eliminarUsuario')->name('usuarioDelete');

//Agregar empleado
Route::post('/newEmpleado', 'empleadoController@newEmpleado')->name('newEmpleado');
//Obtener empleados de la BD
Route::get('/empleados', 'empleadoController@obtenerEmpleados')->name('empleados');
//Actualizar datos de un empleado en especifico
Route::post('/empleado-edit/{id}', 'empleadoController@actualizarEmpleado')->name('actualizarEmpleado');
//Eliminar una empleado
Route::post('/empleado-delete/{id}', 'empleadoController@eliminarEmpleado')->name('eliminarEmpleado');

//Agregar sucursal
Route::post('/newSucursal', 'sucursalController@newSucursal')->name('newSucursal');
//Obtener sucursales de la BD
Route::get('/sucursales', 'sucursalController@obtenerSucursales')->name('sucursales');
//Actualizar datos de una sucursal en especifico
Route::post('/sucursal-edit/{id}', 'sucursalController@actualizarSucursal')->name('actualizarSucursal');
//Eliminar una sucursal
Route::post('/sucursal-delete/{id}', 'sucursalController@eliminarSucursal')->name('eliminarSucursal');

//Rutas para catalogos
//Obtener catalogo de areas en la BD
Route::get('/areas', 'sucursalController@obtenerAreas')->name('areas');
//Obtener catalogo de puestos en la BD
Route::get('/puestos', 'sucursalController@obtenerPuestos')->name('puestos');


// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
