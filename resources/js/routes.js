import Vue from "vue";
import Router from "vue-router";
Vue.use(Router);


export default new Router({
  routes: [
    { 
      path: '/dashboard',
      name: 'dashboard',
      component:  require('./components/Home').default
    },
    { 
      path: '/proveedores',
      name: 'proveedores',
      component:  require('./components/listadoProveedores').default
    },
    { 
      path: '/registroProveedor',
      name: 'registroProveedor',
      component:  require('./components/registroElemento').default
    },
    { 
      path: '/getEmpleados',
      name: 'getEmpleados',
      component:  require('./components/listadoEmpleados').default
    },
    { 
      path: '/getSucursales',
      name: 'getSucursales',
      component:  require('./components/sucursalesComponent').default
    },
    { 
      path: '/registroUsuario',
      name: 'registroUsuario',
      component:  require('./components/Usuarios').default
    },
    { 
      path: '/importar',
      name: 'importar',
      component:  require('./components/ImportExcel').default
    },
  ],
  mode: 'history',
  linkActiveClass: 'is-active',
  base: process.env.MIX_BASE_URL
})

