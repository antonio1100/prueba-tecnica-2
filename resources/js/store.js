import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        loggedin: false
    },
    mutations:{
        loggedin (state) {
            state.loggedin = true
        }
    },
    actions: {
        mockLogin (context) {
            setTimeout(function() {
                context.commit('loggedin')
            }, 100)
        }
    }
})