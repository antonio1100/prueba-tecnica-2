require('./bootstrap');
window.Vue = require('vue');


// import App from "./App.vue";
import 'vuetify/dist/vuetify.min.css'

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('inicio-component', require('./components/inicio.vue').default);
Vue.component('login-component', require('./components/Login.vue').default);
Vue.component('app-component', require('./components/App.vue').default);
import Vue from "vue";
import store from './store'
import router from './routes'
import Vuetify from 'vuetify';
Vue.use(Vuetify);


router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
      // this route requires auth, check if logged in
      // if not, redirect to login page.
      if (!store.state.loggedin) {
        next('login')
      } else {
        next()
      }
    } else {
      next() // make sure to always call next()!
    }
  })

const app = new Vue({
    el: '#app',
    vuetify: new Vuetify(),
    router,
    // render: h => h(App)
  
});