<?php

use Illuminate\Database\Seeder;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'  => 'Administrator',
            'rol'   => 'ADMINISTRADOR',
            'email'      => 'admin@domain.com',
            'password'   =>  bcrypt('123456789')
        ]);
    }


    
}
