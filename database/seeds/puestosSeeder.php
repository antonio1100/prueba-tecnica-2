<?php

use Illuminate\Database\Seeder;

class puestosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('puestos')->insert([
            ['nombre'=>'GERENTE'],
            ['nombre'=>'SUBGERENTE'],
            ['nombre'=>'SUPERVISOR'],
            ['nombre'=>'AUXILIAR GENERAL'],
          
        ]);
    }
}
