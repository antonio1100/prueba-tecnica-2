<?php

use Illuminate\Database\Seeder;

class areasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('areas')->insert([
            ['nombre'=>'AREA DE CAJAS'],
            ['nombre'=>'DAMAS'],
            ['nombre'=>'CABALLEROS'],
            ['nombre'=>'ELECTRONICA'],
            ['nombre'=>'SALCHICHONERIA'],
            ['nombre'=>'ABARROTES'],
            ['nombre'=>'FERRETERIA'],
            ['nombre'=>'ZAPATERIA']
          
        ]);
    }
}
