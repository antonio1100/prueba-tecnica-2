<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->text('telefono')->nullable();
            $table->string('email')->unique();
            $table->string('domicilio',600);
            $table->bigInteger('area_id')->unsigned();
            //$table->foreign('area_id')->references('id')->on('cat_areas');
            $table->bigInteger('puesto_id')->unsigned();
            //$table->foreign('puesto_id')->references('id')->on('cat_puestos');
            $table->bigInteger('sucursal_id')->unsigned();
            //$table->foreign('sucursal_id')->references('id')->on('sucursales');
            $table->date('fecha_ingreso');
            $table->boolean('estatus')->default(1); // 0 = no activo && 1 = activo
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleados');
    }
}
