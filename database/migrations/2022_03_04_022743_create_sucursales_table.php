<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSucursalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sucursales', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',250)->nullable();
            $table->string('tipo',); //sucursal o matriz
            $table->text('telefono')->nullable();
            $table->string('email_contacto')->unique();
            $table->string('nombre_encargado',250)->nullable();
            $table->string('domicilio',600);
            $table->boolean('estatus')->default(1); // 0 = no activo && 1 = activo

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sucursales');
    }
}
