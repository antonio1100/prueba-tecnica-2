<?php

namespace App\Imports;
use App\Proveedor;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ArchivosImport implements ToModel,WithStartRow
{
    //configurando la fila de lectura de inicio
    public function startRow(): int
    {
  
        return 2;
    }

    public function model(array $row)
    {
        // dd($row);
        return new Proveedor([
            'nombre_completo' => $row[0],
            'RFC' => $row[1],
            'email' => $row[2]
        ]);

    }
}
