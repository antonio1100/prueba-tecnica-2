<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class empleado extends Model
{
    protected $table = 'empleados';

    protected $fillable = [
        'id',
        'nombre',
        'telefono',
        'email',
        'domicilio',
        'area_id',
        'puesto_id',
        'sucursal_id',
        'fecha_ingreso',
        'estatus'
    ];

    public function sucursal()
    {
        return $this->hasOne('App\Models\Sucursal','id','sucursal_id');
    }
}
