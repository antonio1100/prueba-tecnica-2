<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\empleado;

class empleadoController extends Controller
{
    public function newEmpleado(Request $request){
        // dd($request);
        $nuevo_empleado = new empleado;
        $nuevo_empleado->nombre = $request->nombre;
        $nuevo_empleado->telefono = $request->telefono;
        $nuevo_empleado->email = $request->email;
        $nuevo_empleado->domicilio = $request->calle.','.$request->numero.','.$request->colonia.','.$request->cp.','.$request->ciudad.','.$request->estado;
        $nuevo_empleado->area_id = $request->area;
        $nuevo_empleado->puesto_id = $request->puesto;
        $nuevo_empleado->sucursal_id = $request->sucursal;
        $nuevo_empleado->fecha_ingreso = $request->fechaIngreso;
        $nuevo_empleado->estatus = 1;
        $nuevo_empleado->save();

        return response()->json(['response' => 'success', 'status' => 1],200);

    }

    public function obtenerEmpleados(Request $request){
        $empleados= empleado::with('sucursal')->get();

        return response()->json([
            'empleados' => $empleados],200);
    }

    public function actualizarEmpleado(Request $request,$id){
        // dd($request);
        $upd_empleado = empleado::find($id);
        $upd_empleado->nombre = $request->nombre;
        $upd_empleado->telefono = $request->telefono;
        $upd_empleado->email = $request->email;
        $upd_empleado->domicilio = $request->calle.','.$request->numero.','.$request->colonia.','.$request->cp.','.$request->ciudad.','.$request->estado;
        $upd_empleado->area_id = $request->area;
        $upd_empleado->puesto_id = $request->puesto;
        $upd_empleado->sucursal_id = $request->sucursal;
        $upd_empleado->fecha_ingreso = $request->fechaIngreso;
        $upd_empleado->estatus = $request->estatus;
        $upd_empleado->save();

        return response()->json(['response' => 'success', 'status' => 1],200);

    }

    public function eliminarEmpleado($id){
        $delete_empleado = empleado::find($id);
        $delete_empleado->delete();
        
        return response()->json(['response' => 'success', 'status' =>1],200);
    }
}
