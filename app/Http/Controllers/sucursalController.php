<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sucursal;
use App\Models\Cat_areas;
use App\Models\Cat_puestos;

class sucursalController extends Controller
{
    public function newSucursal(Request $request){
   
        $nueva_sucursal = new Sucursal;
        $nueva_sucursal->nombre =  $request->nombre_sucursal;
        $nueva_sucursal->tipo =  $request->tipo;
        $nueva_sucursal->telefono =  $request->telefono;
        $nueva_sucursal->email_contacto =  $request->email;
        $nueva_sucursal->nombre_encargado =  $request->nombre_encargado;
        $nueva_sucursal->domicilio =  $request->calle.','.$request->numero.','.$request->colonia.','.$request->cp.','.$request->ciudad.','.$request->estado;
        $nueva_sucursal->save();

        return response()->json(['response' => 'success', 'status' => 1],200);

    }

    public function obtenerSucursales(){
        $sucursales = Sucursal::all();

        return response()->json([
            'sucursales' => $sucursales ],200);
    }

    public function actualizarSucursal(Request $request,$id){

        $upd_sucursal = Sucursal::find($id);
        $upd_sucursal->nombre =  $request->sucursal;
        $upd_sucursal->tipo =  $request->tipo;
        $upd_sucursal->telefono =  $request->telefono;
        $upd_sucursal->email_contacto =  $request->correo;
        $upd_sucursal->nombre_encargado =  $request->encargado;
        $upd_sucursal->domicilio =  $request->calle.','.$request->numero.','.$request->colonia.','.$request->cp.','.$request->ciudad.','.$request->estado;
        $upd_sucursal->estatus = $request->estatus;
        $upd_sucursal->save();

        return response()->json(['response' => 'success', 'status' => 1],200);

    }

    public function eliminarSucursal($id){
        $delete_sucursal = Sucursal::find($id);
        $delete_sucursal->delete();
        
        return response()->json(['response' => 'success', 'status' =>1],200);

    }

    public function obtenerAreas(){
        $areas = Cat_areas::all();

        return response()->json([
            'areas' => $areas ],200);
    }

    public function obtenerPuestos(){
        $puestos = Cat_puestos::all();

        return response()->json([
            'puestos' => $puestos ],200);
    }
}
