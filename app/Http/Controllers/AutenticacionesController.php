<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use App\Http\Requests\LoginComponentRequest;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Session;
use App\User;


class AutenticacionesController extends Controller
{

    // use AuthenticatesUsers;

    // protected $redirectTo = '/home';

    // public function __construct()
    // {
    //     $this->middleware('guest')->except('logout');
    // }

    public function showLoginForm(){

         
            return view('welcome');


            // return view('auth.login-component');
    }

    public function iniciarSesion(LoginComponentRequest $request){
        

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], false)) {
            return response()->json([$request, 'status' =>1],200);
        }else{
            return response()->json(['errors' => ['login' => ['Los datos que ingreso no coinciden con nuestros registros']]],422);
        }

        
    }  

    public function datosUsuario(){
        $datosUsuario = auth()->user();
        return $datosUsuario;
        // return [
        //     'id_fiscal' => Session::all(),
            // 'nombre' => Session::get('nombres_fiscal'),
            // 'apellidos' => Session::get('apellidos_fiscal'),
            // 'correo' => Session::get('email'),
            // // 'rol' => Session::get('roles_totales')[0]['name'], //DESDE HACE DÍAS DEBIÓ TRONAR PORQUE EN CONTROLADOR DE LOGIN YA NO SE METE EN LA SESIÓN
            // 'rol' => Session::get('roles')[1],
            // 'id_rol' => Session::get('roles')[0],
            // 'abr' => $abr,
            // 'region'=> $datosFiscal->id_region ?? '',
            // 'unidad'=> $datosFiscal->id_unidad  ?? '',
            // 'unidadNombre'=> $datosFiscal->unidad->nombre ?? '',
            // 'regionNombre'=> $datosFiscal->region->nombre ?? '',

        // ];
    }

    
    public function logout(Request $request){

        Auth::logout();

        $request->session()->flush(); // Se elimina toda la información de la sesión
        $request->session()->regenerate();
        $request->session()->invalidate();
        return route('login');
    }
       
        
    
}
