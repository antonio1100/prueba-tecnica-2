<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Proveedor;
use Session;
use App\User;
use Maatwebsite\Excel\Facades\Excel; 
use App\Imports\ArchivosImport;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use Maatwebsite\Excel\HeadingRowImport;
use Illuminate\Support\Facades\Crypt;

class importController extends Controller
{
    public function importExcel(Request $request) {
  
       // Obtener objeto de archivo
       $file = $request->file('file');
       
       //obtener extension del archivo subido
       $extension = $file->getClientOriginalExtension();

       if($extension == 'txt'){
           
       // Abrir el archivo en modo de sólo lectura:
        $archivo = fopen($file,"rb");
    
        // Leer la primera línea:
        $aDatos = fgetcsv( $archivo, 500, ";");
 
        // Tras la lectura anterior, el puntero ha quedado en la segunda línea:
        $aDatos = fgetcsv( $archivo, 500, ";" );
 
        // Volvemos a situar el puntero al principio del archivo:
        fseek($archivo, 0);
 
 
        // Recorremos el archivo completo:
        while( feof($archivo) == false )
        {
               $aDatos = fgetcsv( $archivo, 500, ";");
               if($aDatos[0] == null){
 
               }else{
                      $nuevo_registro = new Proveedor;
                      $nuevo_registro->nombre_completo = $aDatos[0];
                      $nuevo_registro->RFC = ltrim($aDatos[1]);
                      $nuevo_registro->email = $aDatos[2];   
                      $nuevo_registro->save();
 
               }
 
        }

        fclose( $archivo ); 

        return response()->json(['response' => 'success', 'status' =>1],200); 

       }else{
             
              $rows= Excel::import(new ArchivosImport, request()->file('file'));

 
              return response()->json(['response' => 'success', 'status' =>1],200); 
       }
 

   }

       public function getProveedores(){
              $registros = Proveedor::all(); 
         
              return response()->json([
                     'registros' => $registros ],200);
       }

       public function newProveedor(Request $request){
              // dd($request);
              $newRegistro = new Proveedor;
              $newRegistro->nombre_completo = $request->nombre;
              $newRegistro->email = $request->email;
              $newRegistro->RFC = $request->rfc;
              $newRegistro->save();

              return response()->json(['response' => 'success', 'status' =>1],200); 

       }

       public function obtenerDatosSesion(){
              // dd("algo");
              $usuario = User::all();
              $datos_sesion = Session::all();
            
              return Session::all();
          }

          public function newUsuario(Request $request){
              // dd($request);
              $newUsuario = new User;
              $newUsuario->name = $request->nombre;
              $newUsuario->email = $request->email;
              $newUsuario->rol = $request->rol;
              $newUsuario->password = bcrypt($request->contrasena);
              $newUsuario->estatus = 1;
              $newUsuario->save();

              return response()->json(['response' => 'success', 'status' =>1],200); 

       }

       public function obtenerUsuarios(){
              // dd("algo");
              $usuarios = User::all();
            
              return response()->json([
                     'registros' => $usuarios ],200);
          }

       public function obtenerUsuario($id){
       // dd($id);
       $usuario = User::find($id);
       
       return response()->json([
              'registros' => $usuario ],200);
       }

       public function actualizarUsuario(Request $request,$id){
              // dd($request->all());
              $updUsuario = User::find($id);
              $updUsuario->name = $request->nombre;
              $updUsuario->email = $request->email;
              $updUsuario->rol = $request->rol;
              $updUsuario->estatus = $request->estatus;
              $updUsuario->password = bcrypt($request->contrasena);
              $updUsuario->save();

              return response()->json(['response' => 'success', 'status' =>1],200); 
       }

       public function eliminarUsuario($id){
              // dd($id);
              $usuario = User::find($id);
              $usuario->delete();
              
              return response()->json(['response' => 'success', 'status' =>1],200);
       }

    
}
